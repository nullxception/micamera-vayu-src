.class public final Lcom/faceunity/core/avatar/entity/FUACompareData;
.super Ljava/lang/Object;
.source "FUACompareData.kt"


# annotations
.annotation runtime LOooO0o0/o0000Ooo;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0002\u0008\u0013\u0018\u00002\u00020\u0001B\u0007\u00a2\u0006\u0004\u0008U\u0010VR5\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0006\u0010\u0007\u001a\u0004\u0008\u0008\u0010\tRU\u0010\u000c\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\nj\u0008\u0012\u0004\u0012\u00020\u0003`\u000b0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\nj\u0008\u0012\u0004\u0012\u00020\u0003`\u000b`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008\u000c\u0010\u0007\u001a\u0004\u0008\r\u0010\tR5\u0010\u000f\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008\u000f\u0010\u0007\u001a\u0004\u0008\u0010\u0010\tR)\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u00110\nj\u0008\u0012\u0004\u0012\u00020\u0011`\u000b8\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u0014\u0010\u0015R\u0085\u0001\u0010\u0019\u001an\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u00050\u0002j6\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0019\u0010\u0007\u001a\u0004\u0008\u001a\u0010\tR\"\u0010\u001b\u001a\u00020\u00038\u0006@\u0006X\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\u001b\u0010\u001c\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R5\u0010!\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00160\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0016`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008!\u0010\u0007\u001a\u0004\u0008\"\u0010\tR\u0085\u0001\u0010#\u001an\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u00050\u0002j6\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u0007\u001a\u0004\u0008$\u0010\tRU\u0010\'\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0%j\u0008\u0012\u0004\u0012\u00020\u000e`&0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0%j\u0008\u0012\u0004\u0012\u00020\u000e`&`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008\'\u0010\u0007\u001a\u0004\u0008(\u0010\tRm\u0010*\u001aV\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u00050\u0002j*\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008*\u0010\u0007\u001a\u0004\u0008+\u0010\tR5\u0010,\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020)`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008,\u0010\u0007\u001a\u0004\u0008-\u0010\tRU\u0010.\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008.\u0010\u0007\u001a\u0004\u0008/\u0010\tR5\u00100\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u00080\u0010\u0007\u001a\u0004\u00081\u0010\tR\u0085\u0001\u00102\u001an\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u00050\u0002j6\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u00082\u0010\u0007\u001a\u0004\u00083\u0010\tR5\u00104\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u00084\u0010\u0007\u001a\u0004\u00085\u0010\tR5\u00106\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020)`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u00086\u0010\u0007\u001a\u0004\u00087\u0010\tRU\u00108\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020)0%j\u0008\u0012\u0004\u0012\u00020)`&0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020)0%j\u0008\u0012\u0004\u0012\u00020)`&`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u00088\u0010\u0007\u001a\u0004\u00089\u0010\tRU\u0010:\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\nj\u0008\u0012\u0004\u0012\u00020\u0003`\u000b0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\nj\u0008\u0012\u0004\u0012\u00020\u0003`\u000b`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008:\u0010\u0007\u001a\u0004\u0008;\u0010\tRU\u0010<\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020)0%j\u0008\u0012\u0004\u0012\u00020)`&0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020)0%j\u0008\u0012\u0004\u0012\u00020)`&`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008<\u0010\u0007\u001a\u0004\u0008=\u0010\tRm\u0010>\u001aV\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u00050\u0002j*\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008>\u0010\u0007\u001a\u0004\u0008?\u0010\tRU\u0010@\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008@\u0010\u0007\u001a\u0004\u0008A\u0010\tR\u0085\u0001\u0010B\u001an\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u00050\u0002j6\u0012\u0004\u0012\u00020\u0003\u0012,\u0012*\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0002j\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008B\u0010\u0007\u001a\u0004\u0008C\u0010\tR5\u0010E\u001a\u001e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020D0\u0002j\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020D`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008E\u0010\u0007\u001a\u0004\u0008F\u0010\tRm\u0010G\u001aV\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u00050\u0002j*\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008G\u0010\u0007\u001a\u0004\u0008H\u0010\tR5\u0010I\u001a\u001e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020D0\u0002j\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020D`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008I\u0010\u0007\u001a\u0004\u0008J\u0010\tRm\u0010K\u001aV\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u00050\u0002j*\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)0\u0002j\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020)`\u0005`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008K\u0010\u0007\u001a\u0004\u0008L\u0010\tRU\u0010M\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008M\u0010\u0007\u001a\u0004\u0008N\u0010\tRU\u0010O\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0%j\u0008\u0012\u0004\u0012\u00020\u000e`&0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0%j\u0008\u0012\u0004\u0012\u00020\u000e`&`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008O\u0010\u0007\u001a\u0004\u0008P\u0010\tR)\u0010Q\u001a\u0012\u0012\u0004\u0012\u00020\u00110\nj\u0008\u0012\u0004\u0012\u00020\u0011`\u000b8\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008Q\u0010\u0013\u001a\u0004\u0008R\u0010\u0015RU\u0010S\u001a>\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b0\u0002j\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e0\nj\u0008\u0012\u0004\u0012\u00020\u000e`\u000b`\u00058\u0006@\u0006\u00a2\u0006\u000c\n\u0004\u0008S\u0010\u0007\u001a\u0004\u0008T\u0010\t\u00a8\u0006W"
    }
    d2 = {
        "Lcom/faceunity/core/avatar/entity/FUACompareData;",
        "",
        "Ljava/util/LinkedHashMap;",
        "",
        "Lcom/faceunity/core/entity/FUEmotionBundleData;",
        "Lkotlin/collections/LinkedHashMap;",
        "avatarEmotionPlayMap",
        "Ljava/util/LinkedHashMap;",
        "getAvatarEmotionPlayMap",
        "()Ljava/util/LinkedHashMap;",
        "Ljava/util/ArrayList;",
        "Lkotlin/collections/ArrayList;",
        "sceneUnbindAvatarMap",
        "getSceneUnbindAvatarMap",
        "Lcom/faceunity/core/entity/FUAnimationBundleData;",
        "avatarAnimationPlayMap",
        "getAvatarAnimationPlayMap",
        "Lcom/faceunity/core/avatar/entity/FUASceneData;",
        "sceneRemoveList",
        "Ljava/util/ArrayList;",
        "getSceneRemoveList",
        "()Ljava/util/ArrayList;",
        "",
        "Lkotlin/Function0;",
        "LOooO0o0/o00OOOOo;",
        "avatarParamsMap",
        "getAvatarParamsMap",
        "dataTime",
        "J",
        "getDataTime",
        "()J",
        "setDataTime",
        "(J)V",
        "sceneItemListJsonMap",
        "getSceneItemListJsonMap",
        "sceneParamsMap",
        "getSceneParamsMap",
        "Ljava/util/HashSet;",
        "Lkotlin/collections/HashSet;",
        "avatarBindAnimationParamsExecuteMap",
        "getAvatarBindAnimationParamsExecuteMap",
        "Lcom/faceunity/core/entity/FUBundleData;",
        "avatarBindHandleMap",
        "getAvatarBindHandleMap",
        "sceneUnbindConfigMap",
        "getSceneUnbindConfigMap",
        "sceneUnbindAnimationMap",
        "getSceneUnbindAnimationMap",
        "sceneReplaceMap",
        "getSceneReplaceMap",
        "avatarPriorityParamsMap",
        "getAvatarPriorityParamsMap",
        "sceneReplaceAvatarMap",
        "getSceneReplaceAvatarMap",
        "sceneBindConfigMap",
        "getSceneBindConfigMap",
        "avatarBindAnimationExecuteMap",
        "getAvatarBindAnimationExecuteMap",
        "sceneBindAvatarMap",
        "getSceneBindAvatarMap",
        "avatarUnbindAnimationExecuteMap",
        "getAvatarUnbindAnimationExecuteMap",
        "sceneBindHandleMap",
        "getSceneBindHandleMap",
        "avatarUnbindAnimationMap",
        "getAvatarUnbindAnimationMap",
        "scenePriorityParamsMap",
        "getScenePriorityParamsMap",
        "",
        "bundleRemoveMap",
        "getBundleRemoveMap",
        "sceneUnbindHandleMap",
        "getSceneUnbindHandleMap",
        "bundleAddMap",
        "getBundleAddMap",
        "avatarUnbindHandleMap",
        "getAvatarUnbindHandleMap",
        "avatarBindAnimationMap",
        "getAvatarBindAnimationMap",
        "avatarUnbindAnimationParamsExecuteMap",
        "getAvatarUnbindAnimationParamsExecuteMap",
        "sceneAddList",
        "getSceneAddList",
        "sceneBindAnimationMap",
        "getSceneBindAnimationMap",
        "<init>",
        "()V",
        "lib_core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x4,
        0x2
    }
.end annotation


# instance fields
.field private final avatarAnimationPlayMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;"
        }
    .end annotation
.end field

.field private final avatarBindAnimationExecuteMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarBindAnimationMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarBindAnimationParamsExecuteMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarBindHandleMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarEmotionPlayMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUEmotionBundleData;",
            ">;"
        }
    .end annotation
.end field

.field private final avatarParamsMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final avatarPriorityParamsMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final avatarUnbindAnimationExecuteMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarUnbindAnimationMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarUnbindAnimationParamsExecuteMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final avatarUnbindHandleMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final bundleAddMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final bundleRemoveMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private dataTime:J

.field private final sceneAddList:Ljava/util/ArrayList;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/avatar/entity/FUASceneData;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneBindAnimationMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final sceneBindAvatarMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final sceneBindConfigMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneBindHandleMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final sceneItemListJsonMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneParamsMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final scenePriorityParamsMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final sceneRemoveList:Ljava/util/ArrayList;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/avatar/entity/FUASceneData;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneReplaceAvatarMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneReplaceMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneUnbindAnimationMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final sceneUnbindAvatarMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final sceneUnbindConfigMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;"
        }
    .end annotation
.end field

.field private final sceneUnbindHandleMap:Ljava/util/LinkedHashMap;
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->dataTime:J

    .line 3
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->bundleRemoveMap:Ljava/util/LinkedHashMap;

    .line 4
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->bundleAddMap:Ljava/util/LinkedHashMap;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneRemoveList:Ljava/util/ArrayList;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneAddList:Ljava/util/ArrayList;

    .line 7
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneReplaceMap:Ljava/util/LinkedHashMap;

    .line 8
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindAvatarMap:Ljava/util/LinkedHashMap;

    .line 9
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindAvatarMap:Ljava/util/LinkedHashMap;

    .line 10
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneReplaceAvatarMap:Ljava/util/LinkedHashMap;

    .line 11
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindConfigMap:Ljava/util/LinkedHashMap;

    .line 12
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindConfigMap:Ljava/util/LinkedHashMap;

    .line 13
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneItemListJsonMap:Ljava/util/LinkedHashMap;

    .line 14
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindHandleMap:Ljava/util/LinkedHashMap;

    .line 15
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindHandleMap:Ljava/util/LinkedHashMap;

    .line 16
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindAnimationMap:Ljava/util/LinkedHashMap;

    .line 17
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindAnimationMap:Ljava/util/LinkedHashMap;

    .line 18
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneParamsMap:Ljava/util/LinkedHashMap;

    .line 19
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->scenePriorityParamsMap:Ljava/util/LinkedHashMap;

    .line 20
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindHandleMap:Ljava/util/LinkedHashMap;

    .line 21
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindHandleMap:Ljava/util/LinkedHashMap;

    .line 22
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindAnimationMap:Ljava/util/LinkedHashMap;

    .line 23
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindAnimationMap:Ljava/util/LinkedHashMap;

    .line 24
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarAnimationPlayMap:Ljava/util/LinkedHashMap;

    .line 25
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarEmotionPlayMap:Ljava/util/LinkedHashMap;

    .line 26
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindAnimationExecuteMap:Ljava/util/LinkedHashMap;

    .line 27
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindAnimationParamsExecuteMap:Ljava/util/LinkedHashMap;

    .line 28
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindAnimationExecuteMap:Ljava/util/LinkedHashMap;

    .line 29
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindAnimationParamsExecuteMap:Ljava/util/LinkedHashMap;

    .line 30
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarParamsMap:Ljava/util/LinkedHashMap;

    .line 31
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarPriorityParamsMap:Ljava/util/LinkedHashMap;

    return-void
.end method


# virtual methods
.method public final getAvatarAnimationPlayMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarAnimationPlayMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarBindAnimationExecuteMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindAnimationExecuteMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarBindAnimationMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindAnimationMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarBindAnimationParamsExecuteMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindAnimationParamsExecuteMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarBindHandleMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarBindHandleMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarEmotionPlayMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUEmotionBundleData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarEmotionPlayMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarParamsMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarParamsMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarPriorityParamsMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarPriorityParamsMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarUnbindAnimationExecuteMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindAnimationExecuteMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarUnbindAnimationMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindAnimationMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarUnbindAnimationParamsExecuteMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindAnimationParamsExecuteMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getAvatarUnbindHandleMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->avatarUnbindHandleMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getBundleAddMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->bundleAddMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getBundleRemoveMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Lcom/faceunity/core/entity/FUBundleData;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->bundleRemoveMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getDataTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->dataTime:J

    return-wide v0
.end method

.method public final getSceneAddList()Ljava/util/ArrayList;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/avatar/entity/FUASceneData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneAddList:Ljava/util/ArrayList;

    return-object p0
.end method

.method public final getSceneBindAnimationMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindAnimationMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneBindAvatarMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindAvatarMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneBindConfigMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindConfigMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneBindHandleMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneBindHandleMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneItemListJsonMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneItemListJsonMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneParamsMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneParamsMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getScenePriorityParamsMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "LOooO0o0/o00ooOoO/o000O00/OooO00o<",
            "LOooO0o0/o00OOOOo;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->scenePriorityParamsMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneRemoveList()Ljava/util/ArrayList;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/avatar/entity/FUASceneData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneRemoveList:Ljava/util/ArrayList;

    return-object p0
.end method

.method public final getSceneReplaceAvatarMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneReplaceAvatarMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneReplaceMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneReplaceMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneUnbindAnimationMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Lcom/faceunity/core/entity/FUAnimationBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindAnimationMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneUnbindAvatarMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindAvatarMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneUnbindConfigMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindConfigMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final getSceneUnbindHandleMap()Ljava/util/LinkedHashMap;
    .locals 0
    .annotation build LOooO0oo/OooO0OO/OooO00o/OooOOO;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/faceunity/core/entity/FUBundleData;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->sceneUnbindHandleMap:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public final setDataTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/faceunity/core/avatar/entity/FUACompareData;->dataTime:J

    return-void
.end method
