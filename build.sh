#!/bin/bash
self=$(realpath "$(dirname "$0")")

apktool() {
    java -jar bin/apktool-2.7.1-nxc.jar $@
}
signer() {
    java -jar bin/uber-apk-signer-1.3.0.jar $@
}

sign() {
    signer -a src/dist/MiuiCamera.apk \
        --ks android.jks --ksAlias android --ksPass android --ksKeyPass android \
        --overwrite
}

makezip() {
    moddir=magisk
    rm -rf $moddir/system/priv-app/MiuiCamera
    mkdir -p $moddir/system/priv-app/MiuiCamera
    cp src/dist/MiuiCamera.apk $moddir/system/priv-app/MiuiCamera
    chmod 644 $moddir/system/priv-app/MiuiCamera/MiuiCamera.apk
    cd $moddir
    zip -r BiancaLeica.zip *
    cd ../
    mkdir -p dist
    mv $moddir/BiancaLeica.zip dist
}

cd $self
apktool b src

if [ -f src/dist/MiuiCamera.apk ]; then
    sign
    makezip   
fi
